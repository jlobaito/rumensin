// When you change APPName, be sure to update it in mylibs/util.js
// @see http://paulirish.com/2009/markup-based-unobtrusive-comprehensive-dom-ready-execution/
var APPNAME = {

  initSlider: function() {
    $('.flexslider').flexslider({
      animation: "slide"
    });
  },
  
  // Initializers
  common: {
    init: function() { 

    },
    finalize: function() {

    }
  },
  
  has_slider: {
    init: function() { 
      APPNAME.initSlider();
    },
    finalize: function() { 

    }
  }
};

UTIL = {
  fire: function( func,funcname, args ) {
    var namespace = APPNAME;  // indicate your obj literal namespace here

    funcname = ( funcname === undefined ) ? 'init' : funcname;
    if ( func !== '' && namespace[ func ] && typeof namespace[ func ][ funcname ] == 'function' ) {
      namespace[ func ][ funcname ]( args );
    }
  },
  loadEvents: function() {
    var bodyId = document.body.id;

    // hit up common first.
    UTIL.fire( 'common' );

    // do all the classes too.
    $.each( document.body.className.split( /\s+/ ), function( i, classnm ) {
      UTIL.fire( classnm );
      UTIL.fire( classnm, bodyId );
    });
    UTIL.fire( 'common', 'finalize' );
  }
};

$(document).ready(UTIL.loadEvents);


/* Youtube API */

function youTubeApi(target){
  var tag = document.createElement('script');
  var target = $(target);
  tag.src = "http://www.youtube.com/player_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  var player;
  function onYouTubePlayerAPIReady() {
    var $m = $(target);
    player = new YT.Player('movie_player', {
      playerVars: { 'autoplay': 1, 'controls': 0,'autohide':1,'wmode':'opaque', 'loop': 1, 'rel':0, 'showinfo':0, 'fs':0,'playlist':$m.data('video') },
      videoId: $m.data("video"),
      events: {
        'onReady': onPlayerReady}
      });
  }
}
  

// Hide Header on on scroll down

function hasScrolled() {

  var didScroll;
  var lastScrollTop = 0;
  var delta = 5;
  var navbarHeight = $('header').outerHeight();
  console.log(navbarHeight);

  $(window).scroll(function(event){
    didScroll = true;
  });

  setInterval(function() {
    if (didScroll) {
      hasScrolled();
      didScroll = false;
    }
  }, 250);

  var st = $(this).scrollTop();
  // Make sure they scroll more than delta
  if(Math.abs(lastScrollTop - st) <= delta)
    return;
  
  // If they scrolled down and are past the navbar, add class .nav-up.
  // This is necessary so you never see what is "behind" the navbar.
  if (st > lastScrollTop && st > navbarHeight){
      // Scroll Down
      $('header').removeClass('nav-down').addClass('nav-up').css('top',-navbarHeight);
    } else {
      // Scroll Up
      if(st + $(window).height() < $(document).height()) {
        $('header').removeClass('nav-up').addClass('nav-down').css('top','0');
      }
    }

    lastScrollTop = st;
}

    /* Flex Destroy */
    function flexdestroy(selector) {
        var el = $(selector);
        var elClean = el.clone();

        elClean.find('.flex-viewport').children().unwrap();
        elClean
            .removeClass('flexslider')
            .find('.clone, .flex-direction-nav, .flex-control-nav')
            .remove()
            .end()
            .find('*').removeAttr('style').removeClass(function (index, css) {
                return (css.match(/\bflex\S+/g) || []).join(' ');
            });

        elClean.insertBefore(el);
        el.remove();
    }

    /* Wrap Iframes in Responsive Emebed */
    function iframeEmbed(selector){
      var elem = $(selector);
      elem.wrap( "<div class='video-container'></div>" );
    }

    /* Velocity Page Load Animation */

    var isInView = function ($element) {
        var win = $(window);
        var obj = $element;
        var scrollPosition = win.scrollTop();
        var visibleArea = win.scrollTop() + win.height() + obj.data('offset') + 200;
        var objEndPos = (obj.offset().top + obj.outerHeight());

        return (visibleArea >= objEndPos && scrollPosition <= objEndPos ? true : false)
    };

  function animateContent(){
      $('section, footer .footer-contain').not('.hero').each(function(){
        var offset = $(this).outerHeight() * 0.875;
        $(this).addClass('fade').attr('data-offset',offset);
      });

      $('.item').addClass('content-animate');

      $(window).ready(function(){
        $('section:first').addClass('fade-in').find('.content-animate').velocity('transition.slideRightIn',{
           stagger:150,
           delay:500,
           visibility:'visible'
        });
      });

      $(window).on('scroll load', function () {
        $('section,footer .footer-contain').each(function (key, val) {
            var $el = $(this);

            if(!$el.hasClass("fade-in") && isInView($el)) {
              $el.addClass('fade-in').find('.content-animate').velocity('transition.slideRightIn',{
                 stagger:150,
                 delay:500,
                 visibility:'visible'
              });
            }

            if(!$el.hasClass("fade-in") && isInView($el)) {
              $el.addClass('fade-in').find('.content-animate').velocity('transition.slideRightIn',{
                 stagger:150,
                 delay:500,
                 visibility:'visible'
              });
            }

        });
    });

  }

  /*
  Function Timeline
  */
  function timeline(block){

  var timelineBlocks = $(block),
    offset = 0.8;

  //hide timeline blocks which are outside the viewport
  hideBlocks(timelineBlocks, offset);

  //on scolling, show/animate timeline blocks when enter the viewport
  $(window).on('scroll', function(){
    (!window.requestAnimationFrame) 
      ? setTimeout(function(){ showBlocks(timelineBlocks, offset); }, 100)
      : window.requestAnimationFrame(function(){ showBlocks(timelineBlocks, offset); });
  });

  function hideBlocks(blocks, offset) {
    blocks.each(function(){
      if (
        ( $(this).offset().top > $(window).scrollTop()+$(window).height()*offset ) && $(this)
        ) {
        $(this).find('.cd-timeline-img, .cd-timeline-content').addClass('is-hidden');
      } else {
        $(this).find('.cd-timeline-img, .cd-timeline-content').addClass('is-visible');
      }
      
    });
  }

  function showBlocks(blocks, offset) {
    var i;
    blocks.each(function(){
      ( $(this).offset().top <= $(window).scrollTop()+$(window).height()*offset && $(this).find('.cd-timeline-img').hasClass('is-hidden') ) && $(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('is-hidden').addClass('bounce-in').find('.cd-date').addClass('animate').next('.img-contain').delay(250).queue(function(){
        $(this).addClass('animate');
      });
    });
  }
}

/*
  Set Session Cookie
  */

  function sessionCookie(key){
    if ($.cookie(key) != 'true'){
      /*$('#popup').modal('show');*/
      $.cookie(key, 'true', { expires: 15 });
    }
  }


  

  function popupVideo(wrapper,element){
    var elem = $(element),
        wrapper = $(wrapper);

    $('.playVideo').click(function(){
      wrapper.velocity("fadeOut", { duration: 1000 })
      elem.velocity("fadeIn", {  delay: 999, duration: 1000 })
    })
  }

  function progressBar(){
    var winHeight = $(window).height(),
      docHeight = $(document).height(),
      progressBar = $('progress'),
      max = docHeight - winHeight;

    $('progress').attr('max', max);
    var value = $(window).scrollTop();
    $('progress').attr('value', value);

    $(document).on('scroll', function() {
      value = $(window).scrollTop();
      progressBar.attr('value', value);
    });

    $(document).on('ready', function() {  
      var winHeight = $(window).height(), 
          docHeight = $(document).height(),
          progressBar = $('progress'),
          max, value;

      /* Set the max scrollable area */
      max = docHeight - winHeight;
      progressBar.attr('max', max);

      $(document).on('scroll', function(){
         value = $(window).scrollTop();
         progressBar.attr('value', value);
      });
    });
  }


  /* Init */

function pageLoad(){
    $(".fancybox").fancybox();

    $(".gallery li").lazyload({
      effect : "fadeIn"
    });


    $("#mobile-menu").menumaker({
        format: "multitoggle"
      });
    $('li.has-sub > a').attr('href',"");

    iframeEmbed('iframe');
    sessionCookie('visited');
    timeline('.cd-timeline-block');
    progressBar();
}


$(document).ready(function(){
  pageLoad();
  
})